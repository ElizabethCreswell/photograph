# The Use of Photograph

Photograph is your solution for generating preview or thumbnails of your web-based documents such as Google spreadsheet. It will give you the preview that will show the exact content of the document, providing you the understandable and easy to read file. Photograph also supports cropping of the preview or thumbnail to avoid reworking afterward.

Check out more benefits of Photograph by going [here](https://www.zipngoinsurance.com).

## Installation

Add this line to your application's Gemfile:

    gem 'photograph'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install photograph

## Usage

Photograph can be used either directly through the Photograph::Artist
class or by its little sinata app. 

    @artist = Photograph::Artist.new("http://github.com")
    @artist.shoot!

    @artist.image
    # => MiniMagick instance you can toy with

Or 

    $ bundle exec photograph -h 127.0.0.1 -p 4567

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Added some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Credits


Photograph is maintained and funded by Tactilize.

Contributors : 

* Jean-Hadrien Chabran

The names and logos for Tactilize are trademarks of Tactilize.

## License

Photograph is Copyright © 2012 Tactilize. It is free software, and may be redistributed under the terms specified in the MIT-LICENSE file.